# go-image-test, a test service

This simple program is meant to be used to demonstrate scaling using Docker
containers. When called it returns a yin/yang image with differing colors based
on the hash of the hostname (container) that serves the image.

## Building

The build is taken care of by the GitLab CI. It builds the image using the
build container and pushes the resulting minified container to the GitLab
provided Docker registry.

## Running

After that running the container can be done with

~~~bash
docker run --rm -p 80:80 registry.gitlab.com/sirile/go-image-test:master
~~~

Testing the service can be done with

~~~bash
curl http://<ip_of_docker_node>
~~~

## Running and scaling with Docker Compose

Running with Docker Compose can be done with

~~~bash
docker-compose up -d
~~~

Scaling the service to five instances can be done with

~~~bash
docker-compose scale go-image-test=5
~~~

Testing the service

~~~bash
curl http://<ip_of_docker_node>
~~~

## Building manually

For building the container
[golang-builder](https://github.com/CenturyLinkLabs/golang-builder) is used. The
container can be built (assuming you have a working Docker environment) with:

~~~bash
docker run --rm -v "$(pwd):/src" -v /var/run/docker.sock:/var/run/docker.sock centurylink/golang-builder
~~~

If you want to minimize and tag the image straight after the build the command
is

~~~bash
docker run --rm -v "$(pwd):/src" -v /var/run/docker.sock:/var/run/docker.sock -e COMPRESS_BINARY=true centurylink/golang-builder sirile/go-image-test
~~~
