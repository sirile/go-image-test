/*
A simple test program that returns a yin/yang image with varying colour
depending on a hash calculated from the hostname. Useful for demonstrating
scaling in for example a container based runtime environment.
*/
package main // import "gitlab.com/sirile/go-image-test"

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

var colour string
var hostname string

func main() {
	// Get the hostname and calculate a hash based on it
	name, err := os.Hostname()
	hostname = name
	hash := 0
	for i := 0; i < len(name); i++ {
		hash = int(name[i]) + ((hash << 5) - hash)
	}
	// Generate a colour based on the hostname hash
	colour = "#"
	for i := uint(0); i < 3; i++ {
		hex := "00" + fmt.Sprintf("%x", ((hash>>i*8)&0xFF))
		colour += hex[len(hex)-2:]
	}

	if err == nil {
		log.Printf("Serving image, host: %s, colour: %s", hostname, colour)
		http.HandleFunc("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {})
		http.HandleFunc("/", handler)
		http.ListenAndServe(":80", nil)
	}
}

// Serve the content
func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<!DOCTYPE html><html><head>"+
		"<title>Go scaling demo, go!</title></head><body>"+
		"<h1>%s</h1>"+
		"<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 400 400\">"+
		"<circle cx=\"50\" cy=\"50\" r=\"48\" fill=\"%s\" stroke=\"#000\"/>"+
		"<path d=\"M50,2a48,48 0 1 1 0,96a24 24 0 1 1 0-48a24 24 0 1 0 0-48\" fill=\"#000\"/>"+
		"<circle cx=\"50\" cy=\"26\" r=\"6\" fill=\"#000\"/>"+
		"<circle cx=\"50\" cy=\"74\" r=\"6\" fill=\"#FFF\"/>"+
		"</svg>"+
		"</body></html>", hostname, colour)
	log.Printf("Served image, host: %s, colour: %s", hostname, colour)
}
